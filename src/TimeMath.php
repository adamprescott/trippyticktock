<?php
/**
 * Created by IntelliJ IDEA.
 * User: adam
 * Date: 21/07/18
 * TimeMath: 16:28
 */

namespace adamprescott\TrippyTickTock;


use adamprescott\TrippyTickTock\Domain\Attribute;
use adamprescott\TrippyTickTock\Domain\Time;

class TimeMath
{
    /** @var Attribute  */
    protected $standardAttribute;

    /** @var Attribute[] */
    protected $additionalDayAttributes;

    public function __construct($secondsPerMin, $minutesPerHour, $hoursPerDay)
    {
        $this->standardAttribute = new Attribute($secondsPerMin, $minutesPerHour, $hoursPerDay);
        $this->additionalDayAttributes = [];
    }

    public function addDayOfWeekAttribute($day, $secondsPerMin, $minutesPerHour, $hoursPerDay)
    {
        $this->additionalDayAttributes[$day] = new Attribute($secondsPerMin, $minutesPerHour, $hoursPerDay);
    }

    /**
     * @param $epoch
     * @return Time
     */
    public function secondsToTime($seconds)
    {
        $attribute = $this->standardAttribute;

        $days = floor($seconds / $this->getSecondsPerUnit('day', $attribute));

        if (key_exists((int)$days, $this->additionalDayAttributes)) {
            $attribute = $this->additionalDayAttributes[$days];
        }

        $hourSeconds = $seconds % $this->getSecondsPerUnit('day', $attribute);
        $hours = floor($hourSeconds / $this->getSecondsPerUnit('hour', $attribute));

        $minuteSeconds = $hourSeconds % $this->getSecondsPerUnit('hour', $attribute);
        $minutes = floor($minuteSeconds / $this->getSecondsPerUnit('minute', $attribute));

        $remainingSeconds = ceil($minuteSeconds % $this->getSecondsPerUnit('minute', $attribute));

        return new Time($remainingSeconds, $minutes, $hours, $days);
    }

    public function getSecondsPerUnit($unit, Attribute $attribute)
    {
        switch (strtolower($unit)) {
            case 'day':
                return (
                    $attribute->getHoursPerDay() *
                    $attribute->getMinutesPerHour() *
                    $attribute->getSecondsPerMin()
                );
                break;
            case 'hour':
                return ($attribute->getMinutesPerHour() * $attribute->getSecondsPerMin());
                break;
            case 'minute':
                return $attribute->getSecondsPerMin();
                break;
            default:
                throw new \InvalidArgumentException("Provide a valid Unit");
        }
    }
}