<?php

namespace adamprescott\TrippyTickTock\Domain;


class Attribute
{
    protected $secondsPerMin;

    protected $minutesPerHour;

    protected $hoursPerDay;

    public function __construct($secondsPerMin, $minutesPerHour, $hoursPerDay)
    {
        $this->secondsPerMin = $secondsPerMin;
        $this->minutesPerHour = $minutesPerHour;
        $this->hoursPerDay = $hoursPerDay;
    }

    public function getSecondsPerMin()
    {
        return $this->secondsPerMin;
    }

    public function getMinutesPerHour()
    {
        return $this->minutesPerHour;
    }

    public function getHoursPerDay()
    {
        return $this->hoursPerDay;
    }
}