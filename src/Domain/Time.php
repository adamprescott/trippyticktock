<?php
/**
 * Created by IntelliJ IDEA.
 * User: adam
 * Date: 21/07/18
 * Time: 16:36
 */

namespace adamprescott\TrippyTickTock\Domain;


class Time
{
    
    protected $seconds;
    protected $minutes;
    protected $hours;
    protected $days;
    
    public function __construct($seconds, $minutes=0, $hours=0, $days=0)
    {
        $this->seconds = $seconds;
        $this->minutes = $minutes;
        $this->hours = $hours;
        $this->days = $days;
    }

    public function getMinutes()
    {
        return $this->minutes;
    }

    public function getHours()
    {
        return $this->hours;
    }

    public function getDays()
    {
        return $this->days;
    }
}