<?php
require __DIR__.'/vendor/autoload.php';
/* Example usage */

$firstVisit = 1532188800; //2018-07-21 16:00:00 (First Visit)
$currentEpochTime = time();

$diff = $currentEpochTime - $firstVisit;

if (isset($_GET['seconds'])) {
    $diff = $_GET['seconds'];
}

$timeMath = new \adamprescott\TrippyTickTock\TimeMath(22, 32, 5.5);
$timeMath->addDayOfWeekAttribute(7, 22, 155, 15);
$time = $timeMath->secondsToTime($diff);

var_dump($time);